const express = require('express');
const cors = require('cors');
const app = express();
const port = 5000;

app.use(cors());
app.use(express.json());

let todos = [];

app.get('/todos', (res) => {
    res.json(todos);
});

app.post('/todos', (req, res) => {
    const todo = { id: todos.length + 1, ...req.body };
    todos.push(todo);
    res.status(201).json(todo);
});

app.put('/todos/:id', (req, res) => {
    const { id } = req.params;
    const index = todos.findIndex(t => t.id.toString() === id);
    if (index >= 0) {
        todos[index] = { ...todos[index], ...req.body };
        res.json(todos[index]);
    } else {
        res.status(404).send('Todo not found');
    }
});

app.delete('/todos/:id', (req, res) => {
    const { id } = req.params;
    todos = todos.filter(t => t.id.toString() !== id);
    res.status(204).send();
});

app.listen(port, () => console.log(`Server running on port ${port}`));
