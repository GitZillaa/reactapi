import React, { useState, useEffect } from 'react';
import axios from 'axios';

function App() {
    const [todos, setTodos] = useState([]);
    const [task, setTask] = useState('');

    useEffect(() => {
        fetchTodos();
    }, []);

    const fetchTodos = async () => {
        const { data } = await axios.get('http://localhost:5000/todos');
        setTodos(data);
    };

    const addTodo = async () => {
        if (task) {
            const { data } = await axios.post('http://localhost:5000/todos', { task });
            setTodos([...todos, data]);
            setTask('');
        }
    };

    const deleteTodo = async (id) => {
        await axios.delete(`http://localhost:5000/todos/${id}`);
        fetchTodos();
    };

    return (
        <div>
            <input value={task} onChange={e => setTask(e.target.value)} />
            <button onClick={addTodo}>Add Task</button>
            {todos.map(todo => (
                <div key={todo.id}>
                    {todo.task} <button onClick={() => deleteTodo(todo.id)}>Delete</button>
                </div>
            ))}
        </div>
    );
}

export default App;
